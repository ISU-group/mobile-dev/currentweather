package com.example.currentweatherdatabinding

data class CoordData(
    val lon: Float,
    val lat: Float
)
