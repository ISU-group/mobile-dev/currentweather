package com.example.currentweatherdatabinding

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import androidx.databinding.DataBindingUtil
import com.example.currentweatherdatabinding.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar
import com.google.gson.GsonBuilder
import java.io.InputStream
import java.net.URL
import java.util.*
import coil.load
import kotlinx.coroutines.*


class MainActivity : AppCompatActivity() {
    lateinit var selectedCity: String
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        /*
        TODO: реализовать отображение  погоды в текстовых полях и картинках
        картинками отобразить облачность и направление ветра
        использовать data binding, библиотеки уже подключены
         */

        init()
    }

    private fun init() {
        val spinner = findViewById<Spinner>(R.id.spinner_cities)
        spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Log.d("SpinnerTag", "Nothing selected")
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedCity = parent?.selectedItem.toString()
                Log.d("SpinnerTag", "Selected item (${parent?.selectedItem}) with id: ${id.toString()}")
                onClick(view!!)
            }
        }
    }

    private suspend fun loadWeather() {
        val weatherURL = resources.getString(R.string.open_weather_map_url, selectedCity, BuildConfig.API_KEY)
        var weather: Weather? = null

        try {
            val stream = URL(weatherURL).content as InputStream

            // JSON отдаётся одной строкой,
            val data = Scanner(stream).nextLine()

            // TODO: предусмотреть обработку ошибок (нет сети, пустой ответ)

            val gson = GsonBuilder().registerTypeAdapter(Weather::class.java, WeatherDeserializer()).create()
            weather = gson.fromJson(data, Weather::class.java)
        } catch (e: Exception) {
            Log.e("WeatherException", e.toString())
            popUpMessage("No Internet access")
        }

        if (weather?.cod == 200) {
            val weatherIconURL = resources.getString(R.string.weather_icon_url, weather.weather.icon)
            binding.weatherIcon.load(weatherIconURL)
            binding.weather = weather
        } else {
            popUpMessage("No Internet access")

        }

        Log.d("WeatherReceivedData", weather.toString())
    }

    private suspend fun popUpMessage(text: String, duration: Int = Snackbar.LENGTH_LONG) = Snackbar
        .make(findViewById(R.id.main_activity), text, duration).show()

    public fun onClick(v: View) {

        // Используем IO-диспетчер вместо Main (основного потока)
        GlobalScope.launch (Dispatchers.IO) {
            loadWeather()
        }
    }
}